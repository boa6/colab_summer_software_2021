import functools
from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from werkzeug.security import check_password_hash, generate_password_hash

# from flaskr.db import get_db
from .models import Person

bp = Blueprint('user', __name__, url_prefix='/user',template_folder='templates')
@bp.route("/")
def hello_world():
    return "<p>Hello, World!</p>"

@bp.route("/template")
def show_template():
  return render_template("user/example.html", some_value="here is a value", first_user=Person.query.first())